package com.devenv571.rabbitmultitenant.component;

import com.devenv571.rabbitmultitenant.data.Tenant;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Slf4j
@Component
public class TenantHandler {

    @Value("${imp.message.exchange:blubb}")
    private String exchange;

    private static final String TENANTS_JSON = "tenants.json";

    private final ObjectMapper mapper = new ObjectMapper();

    private final AmqpAdmin amqpAdmin;

    private final RabbitListenerEndpointRegistry registry;

    @Getter
    private List<Tenant> tenants = new ArrayList<>();

    public TenantHandler(AmqpAdmin amqpAdmin, RabbitListenerEndpointRegistry registry) {
        this.amqpAdmin = amqpAdmin;
        this.registry = registry;
    }

    @EventListener
    public void onAppStarted(ApplicationStartedEvent event) throws IOException {
        Exchange e = new TopicExchange(exchange);
        amqpAdmin.declareExchange(e);

        this.tenants = mapper.readValue(new File(TENANTS_JSON), new TypeReference<List<Tenant>>() {
        });

        for (Tenant tenant : tenants) {
            byte[] decodedKey = Base64.getDecoder().decode(tenant.getSecretKey());
            tenant.setKey(new SecretKeySpec(decodedKey, "AES"));
            addQueueAndBinding(tenant);
        }
    }

    private void addQueueAndBinding(Tenant tenant) {
        Queue queue = new Queue(tenant.getName(), true, false, false);
        Binding binding = new Binding(tenant.getName(), Binding.DestinationType.QUEUE, exchange, tenant.getName(), null);
        amqpAdmin.declareQueue(queue);
        amqpAdmin.declareBinding(binding);

        SimpleMessageListenerContainer listener = (SimpleMessageListenerContainer) registry.getListenerContainer("my-listener");
        listener.addQueues(queue);
    }

    private void removeQueueAndBinding(Tenant tenant) {
        amqpAdmin.deleteQueue(tenant.getName());

        SimpleMessageListenerContainer listener = (SimpleMessageListenerContainer) registry.getListenerContainer("my-listener");
        listener.removeQueueNames(tenant.getName());
    }

    public void addTenant(String tenant) throws IOException, NoSuchAlgorithmException {
        Tenant t = getTenant(tenant);
        if (t != null) {
            log.info("Queue for tenant already added.");
            return;
        }

        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256); // for example
        SecretKey secretKey = keyGen.generateKey();
        t = Tenant.builder().name(tenant).secretKey(Base64.getEncoder().encodeToString(secretKey.getEncoded())).key(secretKey).build();
        addQueueAndBinding(t);
        tenants.add(t);
        mapper.writeValue(new File(TENANTS_JSON), tenants);
    }

    public void removeTenant(String tenant) throws IOException {
        Tenant t = getTenant(tenant);
        if (t == null) {
            log.info("Queue for tenant not found.");
            return;
        }

        removeQueueAndBinding(t);
        tenants.remove(t);
        mapper.writeValue(new File(TENANTS_JSON), tenants);
    }

    public Tenant getTenant(String tenant) {
        for (Tenant t : tenants) {
            if (t.getName().equals(tenant)) {
                return t;
            }
        }

        return null;
    }
}
