package com.devenv571.rabbitmultitenant.component;

import com.devenv571.rabbitmultitenant.data.Tenant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitMessageSender {

    @Value("${imp.message.exchange:blubb}")
    private String exchange;

    private final RabbitTemplate rabbitTemplate;

    public RabbitMessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void send(Tenant tenant, byte[] content, boolean encryptionEnabled) {
        byte[] body;
        if (encryptionEnabled) {
            body = EncryptionUtils.encrypt(tenant.getKey().getEncoded(), content);
        } else {
            body = content;
        }
        rabbitTemplate.convertAndSend(exchange, tenant.getName(), body, m -> {
            m.getMessageProperties().setHeader("encrypted", encryptionEnabled);
            return m;
        });
    }
}
