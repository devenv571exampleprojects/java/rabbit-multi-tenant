package com.devenv571.rabbitmultitenant.component;

import com.devenv571.rabbitmultitenant.data.MessagePayload;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class RabbitMessageListener {

    private final ObjectMapper mapper = new ObjectMapper();

    private final Map<String, Integer> map = new ConcurrentHashMap<>();

    private final TenantHandler tenantHandler;

    public RabbitMessageListener(TenantHandler tenantHandler) {
        this.tenantHandler = tenantHandler;
    }

    @RabbitListener(id = "my-listener")
    public void onMessage(Message message) throws IOException {
        String tenant = message.getMessageProperties().getReceivedRoutingKey();
        boolean encryptionEnabled = message.getMessageProperties().getHeader("encrypted");
        String encrypted = new String(message.getBody());
        byte[] decrypted;
        if (encryptionEnabled) {
            decrypted = EncryptionUtils.decrypt(tenantHandler.getTenant(tenant).getKey().getEncoded(), message.getBody());
        } else {
            decrypted = message.getBody();
        }

        List<MessagePayload> messagePayload = mapper.readValue(decrypted, new TypeReference<List<MessagePayload>>() {
        });
//        log.info("Encrypted: {}", encrypted);
//        log.info("Decrypted: {}", messagePayload.toString());
        map.merge(tenant, 1, Integer::sum);
    }

    @Scheduled(fixedRate = 5000)
    public void updateMap() {
        log.info(map.toString());
    }
}