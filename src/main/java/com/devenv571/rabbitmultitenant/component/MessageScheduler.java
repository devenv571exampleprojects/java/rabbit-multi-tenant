package com.devenv571.rabbitmultitenant.component;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

@Component
public class MessageScheduler {

    @Value("${imp.message.encryption:true}")
    private boolean encryptionEnabled;

    private final TenantHandler tenantHandler;

    private final RabbitMessageSender rabbitMessageSender;

    private byte[] content;

    public MessageScheduler(TenantHandler tenantHandler, RabbitMessageSender rabbitMessageSender) {
        this.tenantHandler = tenantHandler;
        this.rabbitMessageSender = rabbitMessageSender;
    }

    @PostConstruct
    public void setup() throws IOException {
        FileInputStream fis = new FileInputStream("example_small.json");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        IOUtils.copy(fis, byteArrayOutputStream);
        this.content = byteArrayOutputStream.toByteArray();
    }

    @Scheduled(initialDelay = 1000, fixedRate = 1000)
    public void send() {
        tenantHandler.getTenants().forEach(t -> rabbitMessageSender.send(t, content, encryptionEnabled));
    }
}
