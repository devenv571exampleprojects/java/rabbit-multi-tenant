package com.devenv571.rabbitmultitenant.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.crypto.SecretKey;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tenant {

    private String name;

    private String secretKey;

    @JsonIgnore
    private SecretKey key;
}
