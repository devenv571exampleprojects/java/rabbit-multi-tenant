package com.devenv571.rabbitmultitenant.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessagePayload {

    private String tenant;

    private Map<String, Object> meta;
}
