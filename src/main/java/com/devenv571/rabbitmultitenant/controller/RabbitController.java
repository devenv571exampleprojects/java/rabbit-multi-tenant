package com.devenv571.rabbitmultitenant.controller;

import com.devenv571.rabbitmultitenant.component.TenantHandler;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("rabbit")
public class RabbitController {

    private final TenantHandler tenantHandler;

    public RabbitController(TenantHandler tenantHandler) {
        this.tenantHandler = tenantHandler;
    }

    @PostMapping("queues/{tenant}")
    public void exposeQueue(@PathVariable("tenant") String tenant) throws IOException, NoSuchAlgorithmException {
        tenantHandler.addTenant(tenant);
    }

    @DeleteMapping("queues/{tenant}")
    public void deleteQueue(@PathVariable("tenant") String tenant) throws IOException {
        tenantHandler.removeTenant(tenant);
    }

}
