package com.devenv571.rabbitmultitenant.components;

import com.devenv571.rabbitmultitenant.AbstractBenchmark;
import com.devenv571.rabbitmultitenant.component.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openjdk.jmh.annotations.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@RunWith(SpringRunner.class)
public class EncryptionUtilsBenchmarkTest extends AbstractBenchmark {

    @MockBean
    private MessageScheduler messageScheduler;

    @MockBean
    private RabbitMessageSender rabbitMessageSender;

    @MockBean
    private RabbitMessageListener rabbitMessageListener;

    @MockBean
    private TenantHandler tenantHandler;

    private static SecretKey secretKey;

    private static byte[] encryptedContent16Kb;
    private static byte[] encryptedContent128Kb;
    private static byte[] encryptedContent512Kb;
    private static byte[] encryptedContent1024Kb;

    private static byte[] content16Kb;
    private static byte[] content128Kb;
    private static byte[] content512Kb;
    private static byte[] content1024Kb;


    @Before
    public void before() {
        this.setMeasurementIterations(3);
        this.setWarmupIterations(1);
        this.setThreads(20);
        this.setReportFileName("encryption_20_threads");
    }

    @Setup(Level.Trial)
    public void setupBenchmark() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256); // for example

        this.secretKey = keyGen.generateKey();
        this.content16Kb = createDataSize(1024 * 16).getBytes();
        this.content128Kb = createDataSize(1024 * 128).getBytes();
        this.content512Kb = createDataSize(1024 * 512).getBytes();
        this.content1024Kb = createDataSize(1024 * 1024).getBytes();
        this.encryptedContent16Kb = EncryptionUtils.encrypt(this.secretKey.getEncoded(), this.content16Kb);
        this.encryptedContent128Kb = EncryptionUtils.encrypt(this.secretKey.getEncoded(), this.content128Kb);
        this.encryptedContent512Kb = EncryptionUtils.encrypt(this.secretKey.getEncoded(), this.content512Kb);
        this.encryptedContent1024Kb = EncryptionUtils.encrypt(this.secretKey.getEncoded(), this.content1024Kb);
    }

    @Benchmark
    public void encryption16KbBenchmark() {
        EncryptionUtils.encrypt(secretKey.getEncoded(), content16Kb);
    }

    @Benchmark
    public void decryption16KbBenchmark() {
        EncryptionUtils.decrypt(secretKey.getEncoded(), encryptedContent16Kb);
    }

    @Benchmark
    public void encryption128KbBenchmark() {
        EncryptionUtils.encrypt(secretKey.getEncoded(), content128Kb);
    }

    @Benchmark
    public void decryption128KbBenchmark() {
        EncryptionUtils.decrypt(secretKey.getEncoded(), encryptedContent128Kb);
    }

    @Benchmark
    public void encryption512KbBenchmark() {
        EncryptionUtils.encrypt(secretKey.getEncoded(), content512Kb);
    }

    @Benchmark
    public void decryption512KbBenchmark() {
        EncryptionUtils.decrypt(secretKey.getEncoded(), encryptedContent512Kb);
    }

    @Benchmark
    public void encryption1024KbBenchmark() {
        EncryptionUtils.encrypt(secretKey.getEncoded(), content1024Kb);
    }

    @Benchmark
    public void decryption1024KbBenchmark() {
        EncryptionUtils.decrypt(secretKey.getEncoded(), encryptedContent1024Kb);
    }

    private static String createDataSize(int msgSize) {
        StringBuilder sb = new StringBuilder(msgSize);
        for (int i = 0; i < msgSize; i++) {
            sb.append('a');
        }
        return sb.toString();
    }
}
