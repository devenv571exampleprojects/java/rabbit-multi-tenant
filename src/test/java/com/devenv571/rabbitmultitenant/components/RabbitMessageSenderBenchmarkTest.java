package com.devenv571.rabbitmultitenant.components;

import com.devenv571.rabbitmultitenant.AbstractBenchmark;
import com.devenv571.rabbitmultitenant.component.MessageScheduler;
import com.devenv571.rabbitmultitenant.component.RabbitMessageListener;
import com.devenv571.rabbitmultitenant.component.RabbitMessageSender;
import com.devenv571.rabbitmultitenant.component.TenantHandler;
import com.devenv571.rabbitmultitenant.data.MessagePayload;
import com.devenv571.rabbitmultitenant.data.Tenant;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openjdk.jmh.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.KeyGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@RunWith(SpringRunner.class)
public class RabbitMessageSenderBenchmarkTest extends AbstractBenchmark {

    private static RabbitMessageSender rabbitMessageSender;

    @MockBean
    private MessageScheduler messageScheduler;

    @MockBean
    private RabbitMessageListener rabbitMessageListener;

    @MockBean
    private TenantHandler tenantHandler;

    @Autowired
    void setRabbitMessageSender(RabbitMessageSender rabbitMessageSender) {
        RabbitMessageSenderBenchmarkTest.rabbitMessageSender = rabbitMessageSender;
    }

    private static Tenant tenant;

    private static byte[] content1Obj;
    private static byte[] content10Obj;
    private static byte[] content100Obj;
    private static byte[] content1000Obj;


    @Before
    public void before() {
        this.setMeasurementIterations(3);
        this.setWarmupIterations(1);
        this.setThreads(20);
        this.setReportFileName("20_threads");
    }

    @Setup(Level.Trial)
    public void setupBenchmark() throws NoSuchAlgorithmException, JsonProcessingException {
        this.content1Obj = createMessageBody(1);
        this.content10Obj = createMessageBody(10);
        this.content100Obj = createMessageBody(100);
        this.content1000Obj = createMessageBody(1000);

        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256);
        this.tenant = Tenant.builder().name("blubb").key(keyGen.generateKey()).build();
    }

    @Benchmark
    public void send1Benchmark() {
        rabbitMessageSender.send(this.tenant, this.content1Obj, false);
    }

    @Benchmark
    public void send1EncryptedBenchmark() {
        rabbitMessageSender.send(this.tenant, this.content1Obj, true);
    }

    @Benchmark
    public void send10Benchmark() {
        rabbitMessageSender.send(this.tenant, this.content10Obj, false);
    }

    @Benchmark
    public void send10EncryptedBenchmark() {
        rabbitMessageSender.send(this.tenant, this.content10Obj, true);
    }

    @Benchmark
    public void send100Benchmark() {
        rabbitMessageSender.send(this.tenant, this.content100Obj, false);
    }

    @Benchmark
    public void send100EncryptedBenchmark() {
        rabbitMessageSender.send(this.tenant, this.content100Obj, true);
    }

    @Benchmark
    public void send1000Benchmark() {
        rabbitMessageSender.send(this.tenant, this.content1000Obj, false);
    }

    @Benchmark
    public void send1000EncryptedBenchmark() {
        rabbitMessageSender.send(this.tenant, this.content1000Obj, true);
    }

    private static byte[] createMessageBody(int objectCount) throws JsonProcessingException {
        Map<String, Object> meta = new HashMap<>();
        Map<String, String> metaEntry = new HashMap<>();
        metaEntry.put("type", "string");
        metaEntry.put("value", "fdjgfdglkhfdlgkhfdglkhdflg");
        meta.put("m1", metaEntry);
        meta.put("m2", metaEntry);
        meta.put("m3", metaEntry);
        meta.put("m4", metaEntry);
        meta.put("m5", metaEntry);
        meta.put("m6", metaEntry);
        meta.put("m7", metaEntry);
        meta.put("m8", metaEntry);
        meta.put("m9", metaEntry);
        meta.put("m10", metaEntry);

        MessagePayload payload = MessagePayload.builder()
                .tenant("blubb")
                .meta(meta)
                .build();

        List<MessagePayload> list = new ArrayList<>();
        for (int i = 0; i < objectCount; i++) {
            list.add(payload);
        }

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsBytes(list);
    }
}
