package com.devenv571.rabbitmultitenant.components;

import com.devenv571.rabbitmultitenant.AbstractBenchmark;
import com.devenv571.rabbitmultitenant.component.*;
import com.devenv571.rabbitmultitenant.data.MessagePayload;
import com.devenv571.rabbitmultitenant.data.Tenant;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openjdk.jmh.annotations.*;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@RunWith(SpringRunner.class)
public class RabbitMessageListenerBenchmarkTest extends AbstractBenchmark {

    private static RabbitMessageListener rabbitMessageListener;

    @MockBean
    private MessageScheduler messageScheduler;

    @MockBean
    private RabbitMessageSender rabbitMessageSender;

    private static TenantHandler tenantHandler;

    @Autowired
    void setTenantHandler(TenantHandler tenantHandler) {
        RabbitMessageListenerBenchmarkTest.tenantHandler = tenantHandler;
    }

    @Autowired
    void setRabbitMessageListener(RabbitMessageListener rabbitMessageListener) {
        RabbitMessageListenerBenchmarkTest.rabbitMessageListener = rabbitMessageListener;
    }

    private static Tenant tenant;

    private static byte[] encryptedContent1Obj;
    private static byte[] encryptedContent10Obj;
    private static byte[] encryptedContent100Obj;
    private static byte[] encryptedContent1000Obj;

    private static byte[] content1Obj;
    private static byte[] content10Obj;
    private static byte[] content100Obj;
    private static byte[] content1000Obj;

    @Before
    public void before() {
        this.setMeasurementIterations(3);
        this.setWarmupIterations(1);
        this.setThreads(20);
        this.setReportFileName("20_threads");
    }

    @Setup(Level.Trial)
    public void setupBenchmark() throws JsonProcessingException {
        this.tenant = this.tenantHandler.getTenant("tenant1");
        this.content1Obj = createMessageBody(1);
        this.content10Obj = createMessageBody(10);
        this.content100Obj = createMessageBody(100);
        this.content1000Obj = createMessageBody(1000);
        this.encryptedContent1Obj = EncryptionUtils.encrypt(this.tenant.getKey().getEncoded(), this.content1Obj);
        this.encryptedContent10Obj = EncryptionUtils.encrypt(this.tenant.getKey().getEncoded(), this.content10Obj);
        this.encryptedContent100Obj = EncryptionUtils.encrypt(this.tenant.getKey().getEncoded(), this.content100Obj);
        this.encryptedContent1000Obj = EncryptionUtils.encrypt(this.tenant.getKey().getEncoded(), this.content1000Obj);

    }

    @Benchmark
    public void receive1Benchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", false);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.content1Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive1EncryptedBenchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", true);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.encryptedContent1Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive10Benchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", false);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.content10Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive10EncryptedBenchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", true);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.encryptedContent10Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive100Benchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", false);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.content100Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive100EncryptedBenchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", true);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.encryptedContent100Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive1000Benchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", false);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.content1000Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    @Benchmark
    public void receive1000EncryptedBenchmark() throws IOException {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setHeader("encrypted", true);
        messageProperties.setReceivedRoutingKey(this.tenant.getName());
        Message message = new Message(this.encryptedContent1000Obj, messageProperties);

        rabbitMessageListener.onMessage(message);
    }

    private static byte[] createMessageBody(int objectCount) throws JsonProcessingException {
        Map<String, Object> meta = new HashMap<>();
        Map<String, String> metaEntry = new HashMap<>();
        metaEntry.put("type", "string");
        metaEntry.put("value", "fdjgfdglkhfdlgkhfdglkhdflg");
        meta.put("m1", metaEntry);
        meta.put("m2", metaEntry);
        meta.put("m3", metaEntry);
        meta.put("m4", metaEntry);
        meta.put("m5", metaEntry);
        meta.put("m6", metaEntry);
        meta.put("m7", metaEntry);
        meta.put("m8", metaEntry);
        meta.put("m9", metaEntry);
        meta.put("m10", metaEntry);

        MessagePayload payload = MessagePayload.builder()
                .tenant("blubb")
                .meta(meta)
                .build();

        List<MessagePayload> list = new ArrayList<>();
        for (int i = 0; i < objectCount; i++) {
            list.add(payload);
        }

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsBytes(list);
    }
}

